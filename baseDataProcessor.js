// This class (and any its descedant) intends to process a chunk of data.
// It can imply that there will be series of chunks
// but it doesn't deal with a chunk source (a stream etc.) anyway.
class BaseDataProcessor {

  constructor(options) {
    this.foundTimes = 0;
    this.chunksInfo = [];

    if(!options.searchFor) {
      throw '`searchFor` option must be provided!';
    }
    this.searchFor = options.searchFor;
    this.searchForRegexp = new RegExp(this.searchFor, 'gm');
    this.searchForShortenedLength = this.searchFor.length > 1 ? this.searchFor.length - 1 : 0;

    this.prevChunkTail = '';
  }

  processChunk(chunk, processCallback) {
    // console.debug(`Input chunk (${typeof chunk}):\n${chunk}\n`);
    this.chunksInfo.push(chunk.length);

    // Prepend the current chunk with the tail of the previous chunk
    const chunkForProcessing = this.prevChunkTail + chunk;

    // Remember the chunk tail (1 letter less than 1 possible match length) for the next iteration
    this.prevChunkTail = this.getChunkTail(chunkForProcessing);

    // INFO: An alternative solution is also possible:
    // to not remember the chunk tail into this.prevChunkTail (and to not prepend the next chunk with it),
    // but to put the tail back to the readable stream via this.stream.unshift(this.getChunkTail(chunkForProcessing)).
    //
    // It works for `Searcher`. I just had to enclose this code into condition
    // to avoid an infinite loop (when this.stream infinitely emits 'end' event).
    // if(chunk.length >= this.searchFor.length) {
    //   this.stream.unshift(this.getChunkTail(chunkForProcessing));
    // }
    //
    // It doesn't work for `Replacer` as it works inside a Transform stream,
    // where unshift pushes data back into output-side buffer:
    // https://stackoverflow.com/questions/24463300/stream-transform-unshift-in-node-js

    processCallback(chunkForProcessing);
  }

  getChunkWithoutTail(chunk) {
    return chunk.slice(0, -this.searchForShortenedLength);
  }

  getChunkTail(chunk) {
    return chunk.slice(-this.searchForShortenedLength);
  }

  getLastChunkTail() {
    return this.prevChunkTail;
  }

  _found() {
    this.foundTimes++;
  }

  report(appendComment) {
    appendComment = appendComment || '';
    process.stdout.write(`"${this.searchFor}" word was found${appendComment} ${this.foundTimes} times.\n`);
    process.stdout.write(`There were ${this.chunksInfo.length} chunks of data, every chunk was not bigger than ${this.chunksInfo[0]} UTF-8 code units (abstract characters).\n`);
  }

}

module.exports = BaseDataProcessor;
