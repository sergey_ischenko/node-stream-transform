'use strict';

const Profiler = require('./profiler');
const profiler = new Profiler();
profiler.startMeasurement();
const onComplete = () => {
  profiler.stopMeasurement();
};

const trackProgress = (bytes) => {
  profiler.trackProgress(bytes);
};

const srcFile = process.argv[2] || "in.txt";

if(process.env.MODE == 'replace') {
  require('./replace').run({
    inputFile: srcFile,
    searchFor: "Рыба",
    replaceWith: "Селёдка",
    outputFile: "out.txt",
    trackProgress
  }, onComplete);
} else {
  require('./search').run({
    inputFile: srcFile,
    searchFor: "Рыба",
    trackProgress
  }, onComplete);
}
