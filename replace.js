'use strict';

const { Transform } = require('stream');
const fs = require('fs');
const BaseDataProcessor = require('./baseDataProcessor');

class TransformStream extends Transform {
  constructor(options) {
    super({ ...options, decodeStrings: false}); // without this in #_transform `chunk` will be a Buffer, not a String

    this._chunkProcessor = options.chunkProcessor;
    this._trackProgress = options.trackProgress;

    this.on('end', () => {
      this._chunkProcessor.report();
    });
  }

  _transform(chunk, encoding, callback) {
    this._chunkProcessor.processChunk(chunk, (theChunk) => {
      this.push(theChunk);
      this._trackProgress();
      setImmediate(callback);
    });
  };

  _flush(callback) {
    // Every chunk is pushed to `this` stream (see TransformStream#_transform) without its tail (see Replacer#processChunk).
    // Thus after the last chunk from the input stream has been pushed, we additionally need to push the last chunk tail.
    // console.debug(`Input stream has ended. The last chunk tail to be pushed to the output: "${this._replacer.getLastChunkTail()}"`);
    this.push(this._chunkProcessor.getLastChunkTail());

    // Signal the end of the stream (otherwise 'end' event won't happen).
    // There are 2 approaches - both work, I'm not sure which one is better (likely the 2nd):
    // 1) "Passing chunk as null signals the end of the stream (EOF), after which no more data can be written." (https://nodejs.org/dist/latest-v12.x/docs/api/stream.html#stream_readable_push_chunk_encoding)
    // this.push(null);
    // 2) "The callback function must be called when the flush operation is complete." (https://nodejs.org/dist/latest-v12.x/docs/api/stream.html#stream_transform_flush_callback)
    setImmediate(callback);
  }
}

class Replacer extends BaseDataProcessor {
  constructor(options) {
    super(options)

    if(options.replaceWith === undefined) {
      throw '`replaceWith` option must be provided!';
    }
    this.replaceWith = options.replaceWith;
  }

  processChunk(originalChunk, done) {
    super.processChunk(originalChunk, (modifiedChunk) => {
      modifiedChunk = modifiedChunk.replace(this.searchForRegexp, () => {
        this._found();
        return this.replaceWith;
      });

      // Cut off modifiedChunk's tail (it will be processed and pushed out on the next iteration).
      modifiedChunk = this.getChunkWithoutTail(modifiedChunk);
      // It's not necessary for the very last chunk, but I haven't found a way to detect the very last chunk.
      // The problem is solved in TransformStream._flush.

      done(modifiedChunk);
    });
  }

  report() {
    super.report(` and replaced with "${this.replaceWith}"`);
  }
}

const run = (options, done) => {
  const inStream = fs.createReadStream(options.inputFile, {
    encoding: 'utf8',
    highWaterMark: 128*1024
  });

  const replacer = new Replacer({
    searchFor: options.searchFor,
    replaceWith: options.replaceWith
  });
  const transformStream = new TransformStream({
    chunkProcessor: replacer,
    trackProgress: (() => options.trackProgress(inStream.bytesRead))
  });
  transformStream.on('end', done);

  const outStream = fs.createWriteStream(options.outputFile, 'utf8');

  inStream.pipe(transformStream).pipe(outStream);
}

module.exports = { run };
