'use strict';

class Profiler {
  constructor() {
    this.heapUsedHistory = [];
    this.readBytes = 0;
  }

  measure() {
    // process.stdout.write(`Memory usage: ${JSON.stringify(process.memoryUsage())}\n`);
    // process.stdout.write(`CPU usage: ${JSON.stringify(process.cpuUsage())}\n`);

    const info = [];
    info.push(`Processed: ${this.getReadBytesInfo()}`);

    const usedMemory = process.memoryUsage();
    this.heapUsedHistory.push(usedMemory.heapUsed);
    info.push('Memory usage: ');
    for (let key in usedMemory) {
      info.push(`${key}: ${this.asHumanMegabytes(usedMemory[key])}`);
    }

    const usedCPU = process.cpuUsage();
    info.push('CPU usage: ');
    for (let key in usedCPU) {
      info.push(`${key}: ${usedCPU[key]}`);
    }

    console.log([, ...info, ,].join(' | '));
  }

  startMeasurement() {
    this._startTime = process.hrtime();
    setInterval(this.measure.bind(this), 2000).unref();
  }

  stopMeasurement() {
    if(!this.heapUsedHistory.length) {
      this.measure();
    }

    console.log(`Totally processed: ${this.getReadBytesInfo()}.`);
    console.log(`Average heapUsed: ${this.asHumanMegabytes(this.avg(this.heapUsedHistory))}.`);

    const diff = process.hrtime(this._startTime);
    const NS_PER_SEC = 1e9;
    console.log(`Total execution time: ${diff[0] + diff[1]/NS_PER_SEC} seconds.`);
  }

  trackProgress(bytes) {
    this.readBytes = bytes;
  }

  getReadBytesInfo() {
    if(this.readBytes > (1024*1024)) {
      return `${this.asHumanMegabytes(this.readBytes)}`;
    } else {
      return `${this.readBytes} bytes`;
    }
  }

  asHumanMegabytes(bytes) {
    return `${Math.round(bytes / 1024 / 1024 * 100) / 100} MB`;
  }

  avg(arr) {
    return arr.reduce( (p, c) => p + c, 0 ) / arr.length;
  }
}

module.exports = Profiler;
