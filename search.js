'use strict';

const fs = require('fs');
const { Readable } = require('stream');
const BaseDataProcessor = require('./baseDataProcessor');

class ReadStreamProcessor {
  constructor(options) {
    this._chunkProcessor = options.chunkProcessor;
    this._trackProgress = options.trackProgress;
  }

  startPaused(stream) {
    stream.on('end', () => {
      this._chunkProcessor.report();
    });

    // PAUSED MODE
    stream.on('readable', () => {
      // In this mode 'readable' event happens many times
      let chunk;
      while (null !== (chunk = stream.read())) {
        this._chunkProcessor.processChunk(chunk);
        this._trackProgress(stream.bytesRead);
      }
    });
  }

  startFlowing(stream) {
    stream.on('end', () => {
      this._chunkProcessor.report();
    });

    // FLOWING MODE
    // In this mode 'readable' event happens only twice - at the beginning and at the end,
    // and there is no necessity to handle the event.
    // Moreover, listening to it likely makes you "stuck in paused mode"
    // (https://areknawo.com/node-js-file-streams-explained/)?
    stream.on('data', (chunk) => {
      this._chunkProcessor.processChunk(chunk);
      this._trackProgress(stream.bytesRead);
    });
  }

  async startAsync(stream) {
    for await (const chunk of stream) {
      this._chunkProcessor.processChunk(chunk);
      this._trackProgress(stream.bytesRead);
    }

    this._chunkProcessor.report();
  }
}

class Searcher extends BaseDataProcessor {
  constructor(options) {
    super(options);
  }

  processChunk(chunk) {
    super.processChunk(chunk, (theChunk) => {
      this.searchForRegexp.lastIndex = 0; // search from the start of the theChunk

      // test() called multiple times on the same global regular expression instance will advance past the previous match
      while(this.searchForRegexp.test(theChunk)) {
        this._found();
      }
    });
  }
}

const run = (options, done) => {
  const inStream = fs.createReadStream(options.inputFile, {
    encoding: 'utf8',
    highWaterMark: 128*1024
  });
  inStream.on('end', done);

  const searcher = new Searcher({
    searchFor: options.searchFor
  });
  const readStreamProcessor = new ReadStreamProcessor({
    chunkProcessor: searcher,
    trackProgress: options.trackProgress
  });

  // ********** PAUSED stream mode, explicit read() **********
  readStreamProcessor.startPaused(inStream);
  // **********************************************************
  // Stats of execution stats:
  // -------- with highWaterMark 2048 and in.txt 2GB:
  // Average heapUsed: 22.05 MB.
  // Total execution time: 28.633904116 seconds.
  // "Рыба" word was found 110157211 times.
  // There were 1102089 chunks of data, every chunk was not bigger than 1448 UTF-8 code units (abstract characters).
  // -------- with highWaterMark 128*1024 and in.txt 10GB:
  //   Average heapUsed: 10.69 MB.
  // Total execution time: 59.976411304 seconds.
  // "Рыба" word was found 518245529 times.
  // There were 81056 chunks of data, every chunk was not bigger than 92708 UTF-8 code units (abstract characters).

  // ********** async iteration (PAUSED? stream mode) **********
  // readStreamProcessor.startAsync(inStream);
  // **********************************************************
  // Stats of execution stats
  // -------- with highWaterMark 2048 and in.txt 2GB:
  // Average heapUsed: 28.25 MB.
  // Total execution time: 31.120686865 seconds.
  // "Рыба" word was found 110157211 times.
  // There were 1102089 chunks of data, every chunk was not bigger than 1448 UTF-8 code units (abstract characters).
  // -------- with highWaterMark 128*1024 and in.txt 10GB:
  // Average heapUsed: 11.24 MB.
  // Total execution time: 63.23411037 seconds.
  // "Рыба" word was found 518245529 times.
  // There were 81056 chunks of data, every chunk was not bigger than 92708 UTF-8 code units (abstract characters).

  // ********** FLOWING stream mode, callback on 'data' event **********
  // readStreamProcessor.startFlowing(inStream);
  // **********************************************************
  // Stats of execution stats:
  // -------- with highWaterMark 2048 and in.txt 2GB:
  // Average heapUsed: 25.29 MB.
  // Total execution time: 32.584996323 seconds.
  // "Рыба" word was found 110157211 times.
  // There were 1102089 chunks of data, every chunk was not bigger than 1448 UTF-8 code units (abstract characters).
  // -------- with highWaterMark 128*1024 and in.txt 10GB:
  //   Average heapUsed: 11.33 MB.
  // Total execution time: 66.951661429 seconds.
  // "Рыба" word was found 518245529 times.
  // There were 81056 chunks of data, every chunk was not bigger than 92708 UTF-8 code units (abstract characters).
}

module.exports = { run };
