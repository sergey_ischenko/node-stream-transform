## The tasks

1. Count a specified word occurences in a text file.
2. Replace all the specified word occurences from a text file with another word and save the result to another file.

Prerequisites:

A) The file might be very big (so it's not appropriate to load it entirely into the memory).
B) The file might not contain any repeatable delimiters (line breaks etc.), so the logic cannot rely on pre-splitting the stream into portions to process every portion independently on all other portions.

## Implementation notes

Search (the Task #1) is implemented as a Readable stream processor.
Replacement (the Task #2) is implemented as a Transform stream.

### Algorythm of search

In both tasks it was necessary to find a specified word and THERE WAS AN IMPORTANT CHALLENGE.

Initially I analized every chunk (of an arbitrary size) separately, and encountered a problem: **it didn't find the sought-for word, when such a word was split between 2 sequential chunks.**

_One of possible solutions would be to first split the stream into chunks by some delimiter. For example, if the input file is newline delimited and newline isn't a part of the sought-for word, this would work correctly:_

    const rl = require('readline').createInterface({ input: this.stream });
    rl.on('line', (line) => {
        findWordIn(line);
    });

_But, according to Prerequisite #B (see above) THE FILE MIGHT NOT CONTAIN ANY SUITABLE DELIMITERS, what prevents a simple and safe logic of splitting into chunks._

So the following solution is implemented (see BaseDataProcessor#processChunk):
__every chunk (of an arbitrary size), before being processed, is prepended with the previous chunk's tail (such a length which could partially, but not entirely, contain the sought-for word).__

_It works good for a sought-for word of a predictable length (that's when sought-for word is a text). Unfortunately the solution won't work for search by a reqular expression. The only known to me solution for a regular expression is to concatenate incoming chunks until the entire collected buffer either matches to the regular expression (and then the buffer can be partially emptied) or [the buffer limit is reached](https://github.com/dominictarr/split/blob/master/index.js#L45)._

### Experiments with Readable stream modes

In the Task #1 data consuming is implementated with 3 approaches and here are the conclusion:

| The approach                                             | Speed      | Memory usage |
|:---------------------------------------------------------|:----------:|:------------:|
| Pull mode (a.k.a. "PAUSED", doing explicit read())       | The winner | The winner   |
| Async iteration ("PAUSED" stream mode under the hood?)   | 2 place    | 3 place      |
| Push mode (a.k.a. "FLOWING", a callback on 'data' event) | 3 place    | 2 place      |


See more details in search.js.

## How to run

### Search

    node .

### Replacement

    MODE=replace node .


## Benchmarks

The benchmarks were made on 1.4Gb file contained the sought-for word 73593560 times.

### Search

With chunck size 30 bytes:

    $ time node .

    Total execution time: 28.028359812 seconds.
    "Рыба" word was found 73593560 times.
    There were 39522469 chunks of data, every chunk was about 30 UTF-8 code units (abstract characters).

    real  0m28.303s
    user  0m29.713s
    sys 0m1.431s


With chunk size 300 bytes:

    $ time node .

    Total execution time: 14.704124623 seconds.
    "Рыба" word was found 73593560 times.
    There were 3592953 chunks of data, every chunk was about 300 UTF-8 code units (abstract characters).

    real  0m14.959s
    user  0m16.133s
    sys 0m0.898s


Manually with Unix utilities:

    $ time cat in.txt | grep -o Рыба | wc -l
     73593560

    real  0m52.257s
    user  0m52.688s
    sys 0m1.167s


    $ time fgrep -o Рыба in.txt | wc -l
     73593560

    real  0m51.652s
    user  0m51.718s
    sys 0m0.686s

### Replacement

    $ time MODE=replace node .

    Total execution time: 34.443783966 seconds.
    "Рыба" word was found and replaced with "Селёдка" 73593560 times.
    There were 23021 chunks of data, every chunk was about 46356 UTF-8 code units (abstract characters).

    real  0m34.703s
    user  0m34.390s
    sys 0m2.004s


## Extra notes

Every 2 seconds during execution the script outputs CPU and Memory usage information (it's omitted in the Benchmark section above).

## TODO

This implementation doesn't deal explicitly with
[buffering](https://nodejs.org/dist/latest-v12.x/docs/api/stream.html#stream_buffering)
and [backpressuring](https://nodejs.org/es/docs/guides/backpressuring-in-streams/).
